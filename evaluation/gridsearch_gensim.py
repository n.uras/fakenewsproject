# 
# N. Uras - 16 September 2020
# Gensim LDA topic model hyperparameters tuning through gridsearch
#


import os
import gensim
import random
import itertools
import numpy as np
this_dir, _ = os.path.split(__file__)
from gensim.models.coherencemodel import CoherenceModel


class GensimGridSearch(object):
    
    
    def __init__(self, processed_corpus, gensim_corpus, id2word, n_folds, coherence_score):
        # gensim corpus is the corpus obtained apllying id2word to processed corpus
        self.processed_corpus = processed_corpus
        self.gensim_corpus = gensim_corpus
        self.coherence_score = coherence_score
        self.id2word = id2word
        self.n_folds = n_folds
        
        
    def generate_k_folds(self, random_sample=False):
        step = int(len(self.gensim_corpus)/self.n_folds)
        validation_sets = []
        if random_sample:
            for i in range(step, len(self.gensim_corpus)+1, step):
                validation_sets.append(random.sample(self.gensim_corpus, i))
        else:
            segmented_corpus, aggregation = [], []
            for i in range(0, len(self.gensim_corpus), step):
                segmented_corpus.append(self.gensim_corpus[i:i+step])
            randoms = random.sample(range(0, self.n_folds), self.n_folds)
            for idx_fold in range(0, self.n_folds):
                aggregation += segmented_corpus[randoms[idx_fold]]
                validation_sets.append(aggregation)
        return validation_sets
    
    
    def compute_coherence(self, lda_kwargs):
        # gensim lda model instance
        # the argument of the parameter corpus must be a set of validation_sets returned by 
        # generate_k_folds
        # lda_model = gensim.models.LdaMulticore(**lda_kwargs,
        #                                        random_state=42)
        lda_model = gensim.models.wrappers.LdaMallet(**lda_kwargs)
        # compute coherence
        coherence_model_lda = CoherenceModel(model=lda_model, 
                                             texts=self.processed_corpus, 
                                             dictionary=self.id2word, 
                                             coherence=self.coherence_score)
    
        return coherence_model_lda.get_coherence()
    
    
    def __call__(self, hyperparams_grid):
        # hyperparams_grid is a dictionary with hyperparams keys and their searching intervals
        # let's create validation_sets
        val_sets = self.generate_k_folds()
        # unpacking hyperparams_grid dictionary
        keys = sorted(hyperparams_grid)
        combinations = list(itertools.product(*(hyperparams_grid[hyperparam] for hyperparam in keys)))
        hyperparams_list = []
        for comb in combinations:
            d = {keys[i]: comb[i] for i in range(len(comb))}
            hyperparams_list.append(d)
        # dict for saving results
        results, coherence_score = [], []
        for dict_ in hyperparams_list:
            print(f'Hyperparameters configuration {dict_}')
            dict_['id2word'] = self.id2word
            # for mallet lda
            dict_['mallet_path'] = this_dir.replace('evaluation', 'data/mallet-2.0.8/bin/mallet')
            for val_set_idx in range(0, len(val_sets)):
                dict_['corpus'] = val_sets[val_set_idx]
                coherence_score.append(self.compute_coherence(dict_))
            # saving mean coherence calculated over differents val_sets
            dict_['coherence'] = np.mean(coherence_score)
            results.append({k: dict_[k] for k in dict_.keys() if k not in ['corpus', 'id2word', 'mallet_path']})
            # empting list for next hyperparameters configuration
            coherence_score = []
        
        return results
