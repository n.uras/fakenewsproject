#
# N. Uras - 17 September 2020
# This module analyzes the results obtained gridsearching gensim LDA model in
# order to identify the best hyperparameters configuration
#

import os
import joblib
import pandas as pd
from matplotlib import pyplot as plt
this_dir, _ = os.path.split(__file__)

res_umass = joblib.load(os.path.join(this_dir, 'results/u_mass/topics30_100.pkl'))
res_cv = joblib.load(os.path.join(this_dir, 'results/c_v/topics_50-225.pkl'))

df_umass = pd.DataFrame(res_umass)

df_cv = pd.DataFrame(res_cv)

# plotting u_mass results
plt.plot(df_umass['num_topics'], df_umass['coherence'], label='u_mass')
plt.xlabel('topics')
plt.ylabel('coherence')
plt.legend()

# plotting u_mass results
plt.plot(df_cv['num_topics'], df_cv['coherence'], label='c_v')
plt.xlabel('topics')
plt.ylabel('coherence')
plt.legend()
