#
# N. Uras - 15 September 2020
# Test file of gridsearch_gensim module
#


import os
import sys
import joblib
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('evaluation', 'data')
sys.path.insert(0, this_dir.replace('evaluation', 'nlp_preprocessing'))
import gensim.corpora as corpora
from preprocessing import SpacyTokenizer
from gridsearch_gensim import GensimGridSearch

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# from pandas df to list of strings (docs)
corpus = [str(item) for item in corpus_df['text'].unique()]

# processed_corpus = [my_tokenizer(doc) for doc in corpus] # useful line when processed corpus is cumputed for the first time and not saved
processed_corpus = joblib.load(this_dir.replace('evaluation', 'processed_data/processed_corpus_bigrams.pkl'))

# creating data for gensim lda model
# Create Dictionary
id2word = corpora.Dictionary(processed_corpus)
# # Term Document Frequency
gensim_corpus = [id2word.doc2bow(text) for text in processed_corpus]

# creating GensimGridSearch instance
gridsearch = GensimGridSearch(processed_corpus=processed_corpus,
                              gensim_corpus=gensim_corpus,
                              id2word=id2word,
                              n_folds=1,
                              coherence_score='c_v')
# define hyperparameters searching intervals
hyperparams_grid = {'num_topics': [50, 80, 100, 120, 140, 160, 180, 200, 220, 250]}

# optimizing by coherence score with metric c_v
gds_results = gridsearch(hyperparams_grid)

# saving results
metric = 'c_v'
joblib.dump(gds_results, os.path.join(this_dir, f'results/{metric}/topics_50-300_mallet.pkl'))
