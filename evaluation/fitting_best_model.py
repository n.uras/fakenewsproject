# 
# N. Uras - 21 September 2020
# Fitting LDA model with best hyperparamters configuration
#

import os
import sys
import gensim
import joblib
import pandas as pd
import pyLDAvis
import pyLDAvis.gensim
import gensim.corpora as corpora
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('evaluation', 'data')
sys.path.insert(0, this_dir.replace('evaluation', 'nlp_preprocessing'))
from preprocessing import SpacyTokenizer
from gensim.models.coherencemodel import CoherenceModel

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))
corpus = corpus_df.text.values.tolist()

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# processed_corpus = [my_tokenizer(doc) for doc in corpus]

# making bigrams
# processed_corpus = my_tokenizer.make_bigrams(processed_corpus, min_count=5, threshold=100)
processed_corpus = joblib.load(this_dir.replace('evaluation', 'processed_data/processed_corpus.pkl'))
# creating data for gensim lda model
# Create Dictionary
id2word = corpora.Dictionary(processed_corpus)
# Term Document Frequency
gensim_corpus = [id2word.doc2bow(text) for text in processed_corpus]

lda_model = gensim.models.LdaMulticore(corpus=gensim_corpus,
                                        id2word=id2word,
                                        num_topics=90,
                                        random_state=100,
                                        per_word_topics=True)
# compute coherence
coherence_model_lda = CoherenceModel(model=lda_model, 
                                      texts=processed_corpus, 
                                      dictionary=id2word, 
                                      coherence='c_v')

coherence = coherence_model_lda.get_coherence()

# writing results on a txt file
lda_model.show_topics(num_topics=lda_model.num_topics, num_words=10)

# # visualizing lda results
pyLDAvis.enable_notebook()
vis = pyLDAvis.gensim.prepare(lda_model, gensim_corpus, id2word)
pyLDAvis.show(vis)

# # saving topics with top 10 words in text file
file_path = os.path.join(this_dir.replace('evaluation', 'topic_modeling/lda_results'), 'topics_words_90topics.txt')
with open(file_path, 'a') as f:
    for topic_id in range(0, lda_model.num_topics):
        top_words = [word for word, prob in lda_model.show_topic(topic_id, topn=10)]
        print(f'Topic {topic_id}', file=f)
        print(f"{', '.join(top_words)}", file=f)
