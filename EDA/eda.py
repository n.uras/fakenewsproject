#
# N. Uras - 10 September 2020
# EDA module - Exploratory Data Analysis of ThinkTank sample dataset
#

import os
import sys
import seaborn as sns
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('EDA', 'data')
sys.path.insert(0, this_dir.replace('EDA', 'nlp_preprocessing'))
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from preprocessing import SpacyTokenizer
from vectorizers import FromTextToSequences

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# from pandas df to list of strings (docs)
corpus = [str(item) for item in corpus_df['text']]

# create FromTextToSequences instance
vectorizer = FromTextToSequences(corpus)

# fitting vectorizer
args = (my_tokenizer, 'CountVectorizer')
kwargs = {'max_df': 0.95, 'min_df': 1, 'n_gram': (1, 1), 'n_features': 10000}
results = vectorizer.fit_vectorizer(args, kwargs)
words_frequencies = results[2]

# plotting wordcloud
wordcloud = WordCloud(width=900,
                      background_color='white',
                      height=500, 
                      max_words=1000,
                      relative_scaling=0.8,
                      normalize_plurals=False,
                      random_state=42).generate_from_frequencies(words_frequencies)

plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()

# seaborn barplot
pandas_series = pd.Series(words_frequencies)
sorted_series = pandas_series.sort_values(ascending=False)
counts, words = [], []
for i in sorted_series[:20].index:
    counts.append(sorted_series[i])
    words.append(i)
    
sns.barplot(counts, words)
plt.xlabel('counts')
plt.ylabel('Trigrams') # with n_gram set to (3, 3)
