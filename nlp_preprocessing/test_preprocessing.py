#
# N. Uras - 9 September 2020
# Test file of preprocessing module (SpacyTokenizer class)
#

import os
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('nlp_preprocessing', 'data')
from preprocessing import SpacyTokenizer

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# from pandas df to list of strings (docs)
corpus = [str(item) for item in corpus_df['text'].unique()[:150]]

# testing my_tokenizer
processed_corpus = [my_tokenizer(doc) for doc in corpus] 
