#
# N. Uras - 10 September 2020
# Test file of vectorizers module (FromTextToSequences class)
#

import os
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('nlp_preprocessing', 'data')
from preprocessing import SpacyTokenizer
from vectorizers import FromTextToSequences

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# from pandas df to list of strings (docs)
corpus = [str(item) for item in corpus_df['text'].unique()]

# create FromTextToSequences instance
vectorizer = FromTextToSequences(corpus)

# fitting vectorizer
vectorizer_name = 'CountVectorizer'
parameters = {'tokenizer': my_tokenizer, 
              'max_df': 0.95, 
              'min_df': 1, 
              'ngram_range': (1, 2), 
              'max_features': 5000, 
              'lowercase': True}
results = vectorizer.fit_vectorizer(vectorizer_name, parameters)
