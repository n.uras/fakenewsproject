#
# N. Uras - 10 September 2020
# This module implements tf and tf-idf vectorizer
#

import os
import sys
import joblib
this_dir, _ = os.path.split(__file__)
file_path = os.path.join(this_dir, 'fitted_vectorizer')
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

# 2 ways to convert a string to python class object:
# eval(sting)
# getattr(sys.modules[__name__], str)
class FromTextToSequences(object):
    
    
    def __init__(self, corpus):
        # corpus must be a list of strings (docs)
        self.corpus = corpus
        
    
    def __vectorizer(self, vectorizer_name, vectorizer_kwargs):
        # set lowercase to True
        vec = getattr(sys.modules[__name__], vectorizer_name)(**vectorizer_kwargs)
        return vec
    
    
    def fit_vectorizer(self, method, vectorizer_kwargs):
        # create vectorizer instance
        self.vectorizer_instance = self.__vectorizer(method, vectorizer_kwargs)
        # fit vectorizer
        matrix = self.vectorizer_instance.fit_transform(self.corpus)
        # get feature names
        feature_names = self.vectorizer_instance.get_feature_names()
        # saving fitted vectorizer
        joblib.dump(self.vectorizer_instance, os.path.join(file_path, f'{method}.pkl'))
        # words with counts or frequences
        df = pd.DataFrame(matrix.toarray(), columns=feature_names)
        bow = df.sum(axis=0).to_dict()
        # retrieving vocabulary
        vocabulary = self.vectorizer_instance.vocabulary_
        
        return matrix, feature_names, bow, vocabulary
    
    
    def transform_vectorizer(self, corpus, vectorizer_path=None):
        try:
            joblib.load(vectorizer_path)
        except (FileNotFoundError, TypeError):
            fitted_vectorizer = self.vectorizer_instance
            
        return fitted_vectorizer.transform(corpus).toarray()
