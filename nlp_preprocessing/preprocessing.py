#
# N. Uras - 8 September 2020
# This module provides the most relevant and frequent preprocessing steps 
# used in nlp applications.
#


import nltk
import spacy
import gensim
from nltk.corpus import wordnet
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess

# This class must work on single docs of the corpus, in order to be called in this way:
# processed_corpus = [my_tokenizer(doc) for doc in corpus[:10]] 
# doc is a string of the entire corpus

class SpacyTokenizer(object):
    
    
    def __init__(self, language, use_bigrams=True, max_length_text=35649905):
        if language == 'italian':
            self.nlp = spacy.load('it_core_news_sm', disable=['parser', 'ner'])
            self.lang = 'ita'
            self.stopwords_lang = 'italian'
        elif language == 'english':
            self.nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
            self.lang = 'eng'
            self.stopwords_lang = 'english'
        else:
            raise TypeError(f'{language} language is not supported')
        self.nlp.max_length = max_length_text
        self.use_bigrams = use_bigrams
        
    def make_bigrams(self, processed_corpus, min_count, threshold):
        bigram = gensim.models.Phrases(processed_corpus, min_count=min_count, threshold=threshold)
        bigram_mod = gensim.models.phrases.Phraser(bigram)
        processed_corpus = [bigram_mod[doc] for doc in processed_corpus]
        
        return processed_corpus
    
    
    def __call__(self, doc):
        # tokenized_doc = self.nlp(doc)
        # removing punctuation, stopwords, non alphabetic characters and getting lemmas
        # lemmatised_doc = [token.lemma_ for token in tokenized_doc if token.pos_ in 
        #                   ['NOUN', 'ADJ', 'VERB', 'ADV'] and not token.is_stop and not token.is_punct and token.is_alpha]
        # if self.use_bigrams:
        #    lemmatised_doc += [' '.join(bigram) for bigram in list(nltk.bigrams(lemmatised_doc))]
        # lowercase terms
        # lower_doc = [token.lower() for token in lemmatised_doc]
        # processed_doc = []
        # for token in lower_doc:
        #     if token.startswith('coronavirus'):
        #         processed_doc.append(token)
        #     else:
        #         if wordnet.synsets(token, lang=self.lang) and token not in ['e']:
        #             processed_doc.append(token)
        # corretto
        stop_words = stopwords.words(self.stopwords_lang)
        cleaned_doc = [word for word in simple_preprocess(str(doc)) if word not in stop_words]
        tokenized_doc = self.nlp(' '.join(cleaned_doc))
        lemmatised_doc = [token.lemma_ for token in tokenized_doc if token.pos_ in ['NOUN', 'ADJ', 'VERB', 'ADV']]
        if self.use_bigrams:
            lemmatised_doc += [' '.join(bigram) for bigram in list(nltk.bigrams(lemmatised_doc))]
        processed_doc = []
        for token in lemmatised_doc:
            if token.startswith('coronavirus') or token.startswith('covid'):
                processed_doc.append(token)
            else:
                if wordnet.synsets(token, lang=self.lang):
                    processed_doc.append(token)
        
        return  processed_doc 
