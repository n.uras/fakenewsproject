#
# N. Uras - 11 September 2020
# This module includes the TopicModeling class for LDA and NMF models 
# implementation
#

import os
import sys
import joblib
import numpy as np
import pandas as pd
this_dir, _ = os.path.split(__file__)
file_path = os.path.join(this_dir, 'fitted_model')
from sklearn.decomposition import NMF, LatentDirichletAllocation


class TopicModeling(object):
    
    
    def __init__(self, tf_matrix):
        # tf_matrix is the matrix returned by vectorizers
        # features_names is the list of strings (tokens) identified by the vectorizer
        self.tf_matrix = tf_matrix
        
        
    def __model(self, topic_model, lda_nmf_kwargs):
        model_instance = getattr(sys.modules[__name__], topic_model)(**lda_nmf_kwargs)
        
        return model_instance
    
    
    def fit_topic_model(self, topic_model, lda_nmf_kwargs):
        self.model = self.__model(topic_model, lda_nmf_kwargs)
        self.model.fit(self.tf_matrix)
        joblib.dump(self.model, os.path.join(file_path, f'{topic_model}.pkl'))
        
        return self.model.components_
        
    
    def transform_topic_model(self, tf_matrix_test, model_path=None):
        # tf_matrix_test could be the tf_matrix on which the topic model was fitted
        # or a tf_matrix of new documents
        try:
            # using saved model 
            model = joblib.load(model_path)
        except (FileNotFoundError, TypeError):
            model = self.model
        return model.transform(tf_matrix_test)
    
    
    def display_topics(self, n_top_words, model_components, feature_names):
        # model_components: identified topics components. The result returned by fit_topic_model method
        for topic_idx, topic in enumerate(model_components):
            print(f'Topic {topic_idx}')
            # instead array slicing [:-n_top_words -1: -1]
            print(','.join([feature_names[i] for i in topic.argsort()[-n_top_words:][::-1]]))
            
            
    def get_corpus_dominant_topic(self, n_dominants, n_top_words, feature_names, topics_weights, model_components):
        # Apply this method to documents first processed with fitted vectorizer and then
        # transformed with the considered fitted topic model
        # For example, topics_weights will be a matrix representing topics weights 
        # for each document: topics_weights = topic_model.transform(tf_matrix)
        # model_components is the result returned by fit topic model
        # Be careful: this function finds top n topics for new corpus, not for each document
        dominant_topics_indexes = [vector.argmax() for vector in topics_weights]
        unique, counts = np.unique(dominant_topics_indexes, return_counts=True)
        topic_counts = dict(zip(unique, counts))
        df_topics_counts = pd.Series(topic_counts)
        df_topics_counts.sort_values(ascending=False, inplace=True)
        if n_dominants > len(df_topics_counts):
            print(f'Top {len(df_topics_counts)} dominants topics:')
        else:
            print(f'Top {n_dominants} dominants topics:')
        print('\n')
        for i in df_topics_counts[:n_dominants].index:
            print(f'Topic {i}')
            n_top_topic_words_idx = model_components[i].argsort()[-n_top_words:][::-1]
            print(' '.join(feature_names[j] for j in n_top_topic_words_idx))
            
            
    def get_docs_dominant_topics(self, docs_topics_weights):
        # topics_weights: matrix with dim (num_docs, num_topics) returnrd by
        # topic_model.fit(tf_matrix)
        docs_dom_topic = pd.DataFrame()
        docs_dom_topic['dominant_topic'] = [vector.argmax() for vector in docs_topics_weights]
        docs_dom_topic['document_id'] = [f'doc{idx}' for idx in docs_dom_topic.index]
        docs_dom_topic.set_index('document_id', drop=True, inplace=True)
        
        return docs_dom_topic
            