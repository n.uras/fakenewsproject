#
# N. Uras - 11 September 2020
# Test file of vectorizers module (FromTextToSequences class)
#

import os
import sys
import joblib
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('topic_modeling', 'data')
sys.path.insert(0, this_dir.replace('topic_modeling', 'nlp_preprocessing'))
from preprocessing import SpacyTokenizer
from vectorizers import FromTextToSequences
from topic_modeling_module import TopicModeling
from lda_visualization import LdaVisualizationTools

# open dataset
corpus_df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian')

# from pandas df to list of strings (docs)
corpus = [str(item) for item in corpus_df['text'].unique()]

# create FromTextToSequences instance
vectorizer = FromTextToSequences(corpus)

# fitting vectorizer
vec_name = 'CountVectorizer'
vec_params = {'tokenizer': my_tokenizer, 'max_df': 0.95, 'min_df': 1, 'ngram_range': (1, 2), 'max_features': 5000, 'lowercase':True}
results = vectorizer.fit_vectorizer(vec_name, vec_params)

# TopicModeling instance
parameters = {"n_components": 180, # number of topics to identify
              "max_iter": 10,
              "learning_method": 'batch',
              "learning_offset": 10.,
              "random_state": 42}
tf_matrix, feature_names = results[0], results[1]
lda_model = TopicModeling(tf_matrix)
topics = lda_model.fit_topic_model('LatentDirichletAllocation', parameters)
joblib.dump(tf_matrix, os.path.join(this_dir, 'lda_results/tf_matrix.pkl'))
joblib.dump(topics, os.path.join(this_dir, 'lda_results/identified_topics.pkl'))
joblib.dump(feature_names, os.path.join(this_dir, 'lda_results/feature_names.pkl'))
# displaying identified topics
lda_model.display_topics(n_top_words=10, model_components=topics, feature_names=feature_names)
# testing get_docs_dominant_topic method on tf_matrix
docs_topics = lda_model.transform_topic_model(tf_matrix)
docs_dom_topic = lda_model.get_docs_dominant_topics(docs_topics)
docs_dom_topic.to_csv(os.path.join(this_dir, 'lda_results/docs_dominant_topics.csv'))
# looking for n top dominants topics in new corpus
test_corpus = [str(item) for item in corpus_df['text'][51:71]]
# transforming new text into sequences
test_tf_matrix = vectorizer.transform_vectorizer(test_corpus)
# applying lda model to test_tf_matrix
topics_weights = lda_model.transform_topic_model(test_tf_matrix)
# applying get_dominant_topic method
lda_model.get_corpus_dominant_topic(n_dominants=3,
                                    n_top_words=5,
                                    feature_names=feature_names,
                                    topics_weights=topics_weights,
                                    model_components=topics)
# =============================================================================
# Visualizing results - lda_visualization test
# =============================================================================
# loading fitted vectorizer and fitted lda model
fitted_lda = joblib.load(os.path.join(this_dir, 'fitted_model/LatentDirichletAllocation.pkl'))
fitted_vec = joblib.load(this_dir.replace('topic_modeling', 'nlp_preprocessing/fitted_vectorizer/CountVectorizer.pkl'))
vis_tools = LdaVisualizationTools(fitted_lda)
# plotting pyldavis plot
vis_tools.pyldavis_tool(tf_matrix.toarray(), fitted_vec)
# plotting wordcloud with n top words for each topic
vis_tools.wordcloud_tool(feature_names, n_top_words=6, n_rows=2, n_cols=3)
