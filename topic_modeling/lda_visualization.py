#
# N. Uras - 14 September 2020
# This module contains some useful tools for topic modeling visualization.
# Its test script can be found at the end of the test_topic_modeling module
#


import nltk
import pyLDAvis
import numpy as np
import pyLDAvis.sklearn
pyLDAvis.enable_notebook()
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
from wordcloud import WordCloud, STOPWORDS


class LdaVisualizationTools(object):
    
    
    def __init__(self, fitted_lda):
        self.lda = fitted_lda
        self.lda_components = self.lda.components_
        
        
    def pyldavis_tool(self, tf_matrix, fitted_vectorizer):
        vis = pyLDAvis.sklearn.prepare(self.lda, np.matrix(tf_matrix), fitted_vectorizer)
        pyLDAvis.show(vis)
        
        
    def show_topics(self, feature_names, n_top_words):
        # this function returns the same result of show_topics method of lda gensim library 
        # with formatted parameter set to False
        word_prob_per_topic = []
        for id_topic, topic_vec in enumerate(self.lda_components):
            id_top_words = topic_vec.argsort()[-n_top_words:][::-1]
            word_prob_pairs = [(feature_names[i], topic_vec[i]) for i in id_top_words]
            word_prob_per_topic.append((f'topic{id_topic}', word_prob_pairs))
        
        return word_prob_per_topic


    def wordcloud_tool(self, feature_names, n_top_words, n_rows, n_cols):
        # more colors: 'mcolors.XKCD_COLORS'
        cols = [color for name, color in mcolors.TABLEAU_COLORS.items()] 
        stop_words = nltk.corpus.stopwords.words('italian')
        cloud = WordCloud(stopwords=stop_words,
                          background_color='white',
                          width=2500,
                          height=1800,
                          max_words=6, # IMP. must be equal to n_top_words
                          colormap='tab10',
                          color_func=lambda *args, **kwargs: cols[i],
                          prefer_horizontal=1.0)
        # getting words-counts pairs per topic       
        word_prob_per_topic = self.show_topics(feature_names, n_top_words) 
        # plotting wordcloud  
        fig, axes = plt.subplots(n_rows, n_cols, figsize=(10,10), sharex=True, sharey=True)
        for i, ax in enumerate(axes.flatten()):
            fig.add_subplot(ax)
            if i < len(word_prob_per_topic):
                topic_words = dict(word_prob_per_topic[i][1])
                cloud.generate_from_frequencies(topic_words, max_font_size=300)
                plt.gca().imshow(cloud)
                plt.gca().set_title('Topic ' + str(i+1), fontdict=dict(size=16))
                plt.gca().axis('off')
        # deleting last axis when plotting odd number of subplots        
        if n_cols%2 != 0:
            fig.delaxes(axes[1, n_cols-1])
        
        plt.subplots_adjust(wspace=10, hspace=0)
        plt.axis('off')
        plt.margins(x=10, y=10)
        plt.tight_layout()
        plt.show()
