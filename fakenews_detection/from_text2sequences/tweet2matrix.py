#
# N. Uras - 13 Jan 2021
# This python module aims to create tf-idf matrices from tweet's text
#

import os
import sys
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('fakenews_detection/from_text2sequences', 'data')
sys.path.insert(0, this_dir.replace('fakenews_detection/from_text2sequences', 'nlp_preprocessing'))
from preprocessing import SpacyTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer

# open tweets dataset
df = pd.read_csv(os.path.join(data_dir, 'ThinkTank_sample.csv'))

# sample_df = df.head(1000)

# create corpus
corpus = [str(item) for item in df['text']]

# create SpacyTokenizer instance
my_tokenizer = SpacyTokenizer(language='italian', use_bigrams=False)

# Tf-idf Vectorizer instance
tfidf_vec = TfidfVectorizer(tokenizer=my_tokenizer,
                            lowercase=True,
                            analyzer='word',
                            stop_words=None,
                            ngram_range=(1, 2),
                            max_df=0.95,
                            min_df=1,
                            max_features=5000)
# fit tfidf_vec to corpus
tfidf_sequences = tfidf_vec.fit_transform(corpus)

# from sparse matrix to matrix
tfidf_sequences = tfidf_sequences.toarray()

# check duplicates tweets
# obviously, each duplicate tweet must be related to the same topic
# to check this, see dominant_topics.csv file on Gdrive COVID folder
duplicates_idx = []
for i in range(len(corpus)):
    for j in range(len(corpus)):
        if i != j:
            if corpus[i] == corpus[j]:
                duplicates_idx.append((i, j))

# from numbered indexes to vocab indexes dataframe
tfidf_sequences_df = pd.DataFrame(tfidf_sequences, 
                                  columns=[name for name in tfidf_vec.get_feature_names()])

# save tfidf_sequences_df
tfidf_sequences_df.to_csv(os.path.join(this_dir, 'tfidf_sequences.csv'), index=False)
