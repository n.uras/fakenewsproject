#
# N. Uras - 21/01/2021
# Test file for fake news classification
#

import os
import pandas as pd
from models.svm_module import SVM
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, f1_score, recall_score, confusion_matrix
# this_dir is the directory where working_dataset.py file is located
this_dir, _ = os.path.split(__file__)

# open dataset
df = pd.read_csv(os.path.join(this_dir, 'create_dataset/working_dataset_sample.csv'))

df = df.head(50000)

# encoding benford categorical variable
encoding_map = {True: 1, False: 0}
df['benford'] = df['benford'].map(encoding_map)

# dropping topic_id column
df.drop(labels='topic_id', axis=1, inplace=True)

# creating inputs and output
X, y = df.iloc[:, :-1].values, df['label'].values

# splitting into train and test
X_train, X_test, y_train, y_test = train_test_split(X, 
                                                    y, 
                                                    test_size=0.3, 
                                                    random_state=42)

# creating svm instance
params = {'C': 1, 'kernel': 'rbf', 'gamma': 'scale'}
svm = SVM(params)

# fitting svm model
svm.fit_model(X_train, y_train)

# making predictions
y_preds = svm.predict(X_test)

# evaluating the model
accuracy = accuracy_score(y_test, y_preds)
precision = precision_score(y_test, y_preds)
recall = recall_score(y_test, y_preds)
f_1_score = f1_score(y_test, y_preds)

confusion_matrix(y_test, y_preds)
