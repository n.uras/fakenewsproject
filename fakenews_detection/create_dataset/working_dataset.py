#
# N. Uras - 13 Jan 2021
# This python module aims to create a working dataset for fake news detection
# with tfidf sequences, n topic and check on benford's law as inputs and a fake 
# or real target as output
#

import os
import pandas as pd
# this_dir is the directory where working_dataset.py file is located
this_dir, _ = os.path.split(__file__)
tfidf_seq_dir = this_dir.replace('create_dataset', 'from_text2sequences')

# open datasets
dominant_topic_per_tweet = pd.read_csv('dominant_topics.csv')
tfidf_sequences_df = pd.read_csv(os.path.join(tfidf_seq_dir, 'tfidf_sequences.csv'))

# create new column in tfidf_sequences_df
tfidf_sequences_df['n_topic'] = dominant_topic_per_tweet['dominant_topic']

# adding check benford's law column
benford_feature = pd.read_csv('topic_benford.csv')
dominant_topic_per_tweet['benford'] = False
for i in range(dominant_topic_per_tweet.shape[0]):
    if dominant_topic_per_tweet.loc[i, 'dominant_topic'] in list(benford_feature['dominant_topic']):
        dominant_topic_per_tweet.loc[i, 'benford'] = True

# adding labels column
dom_topic_benford = pd.read_csv('dominant_topics_benford.csv')
# open nicola's labels
nicola_labels = pd.read_csv('topics_labels_nicola.csv', sep=';')
# open stefano's labels
stefano_labels = pd.read_csv('topics_labels_stefano.csv', sep=',')
# create labels
nicola_labels['label'] = 0
nicola_labels.loc[(nicola_labels['fake_news'] == 1) & (stefano_labels['fake_news'] == 1), 'label'] = 1
merged = pd.merge(dom_topic_benford, nicola_labels, how='left')
merged_2 = merged[['tweet_id', 'topic_id', 'benford', 'label']]
merged_2.to_csv('topicID_benford_label.csv', index=False)

# save tfidf_sequences_df with dominant topic column as n_topic
tfidf_sequences_df.to_csv(os.path.join(this_dir, 'working_dataset.csv'), index=False)

topic_benford_label = pd.read_csv('topicID_benford_label.csv')
tfidf_sequences_df['topic_id'] = topic_benford_label['topic_id']
tfidf_sequences_df['benford'] = topic_benford_label['benford']
tfidf_sequences_df['label'] = topic_benford_label['label']

tfidf_sequences_df.to_csv('working_dataset.csv', index=False)
