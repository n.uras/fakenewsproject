#
# N. Uras - 3 Feb 2021
# This python module aims to use the keras deep learning library to build 
# a text classifier for fake news detection
#

import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

# open dataset
df = pd.read_csv('create_dataset/dataset_for_transformers.csv')
corpus = df['text'].tolist()

# encoding benford categorical variable
encoding_map = {True: 1, False: 0}
df['benford'] = df['benford'].map(encoding_map)

# process data - tokenization with keras
vocab_size = 5000
tokenizer = Tokenizer(num_words=vocab_size, # the maximum number of words to keep
                      lower=True, # whether to convert the texts to lowercase
                      split=" ") # separator for word splitting
# fit tokenizer on text
tokenizer.fit_on_texts(corpus)
# get sequences
sequences = tokenizer.texts_to_sequences(corpus)

# check sequence with max length to choose maxlen for padding sequences
lengths = [len(sequence) for sequence in sequences]
print(max(lengths))
maxlen = max(lengths) # we deal with quite short sequences

# padding sequences to 200 length
padded_sequences = pad_sequences(sequences, maxlen=maxlen)

# creating inputs and output
X = np.concatenate((padded_sequences, df['benford'].values.reshape(len(df), 1)), axis=1)
y = df['label'].values

# splitting into train and test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# balancing classes with SMOTE
alfa_os, alfa_us = 0.4, 0.7
oversampling = SMOTE(sampling_strategy=alfa_os)
undersampling = RandomUnderSampler(sampling_strategy=alfa_us)
balance_pipe = Pipeline(steps=[('oversample', oversampling), ('undersample', undersampling)])
X_train, y_train = balance_pipe.fit_resample(X_train, y_train)

# EDA after class balancing
y_train_df = pd.DataFrame(y_train, columns=['label'])
y_train_df['label'].value_counts()
sns.countplot(x='label', data=y_train_df)

# shuffling X and y train data in unison
def shuffle_in_unison(a, b):
    assert len(a) == len(b)
    shuffled_a = np.empty(a.shape, dtype=a.dtype)
    shuffled_b = np.empty(b.shape, dtype=b.dtype)
    permutation = np.random.permutation(len(a))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    
    # shuffled_a will be X_train, while shuffled_b will be y_train
    return shuffled_a, shuffled_b

X_train, y_train = shuffle_in_unison(X_train, y_train)

# Implement a transformer block as a layer
class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super(TransformerBlock, self).__init__()
        self.att = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.ffn = keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim),]
        )
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, inputs, training):
        attn_output = self.att(inputs, inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

# implement embedding layer
class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim):
        super(TokenAndPositionEmbedding, self).__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size, output_dim=embed_dim)
        self.pos_emb = layers.Embedding(input_dim=maxlen, output_dim=embed_dim)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions

# create classifier model using transformer layer
embed_dim = 32  # Embedding size for each token
num_heads = 2  # Number of attention heads
ff_dim = 32  # Hidden layer size in feed forward network inside transformer
maxlen = maxlen + 1

inputs = layers.Input(shape=(maxlen,))
embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)
x = embedding_layer(inputs)
transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
x = transformer_block(x)
x = layers.GlobalAveragePooling1D()(x)
x = layers.Dropout(0.1)(x)
x = layers.Dense(20, activation="relu")(x)
x = layers.Dropout(0.1)(x)
outputs = layers.Dense(1, activation="sigmoid")(x)

model = keras.Model(inputs=inputs, outputs=outputs)

# compile and train transformer model
model.compile("adam", "binary_crossentropy", metrics=["accuracy"])
model.fit(X_train, y_train, batch_size=32, epochs=2, validation_data=(X_test, y_test))
    
# checking model performances
losses = pd.DataFrame(model.history.history)
losses[['loss', 'val_loss']].plot()
losses[['accuracy', 'val_accuracy']].plot()

# getting predictions
y_preds = model.predict(X_test)
y_preds = np.argmax(y_preds, axis=-1)

# getting results 
def get_results(**kwargs):
  for kwarg in kwargs:
    print(f'Achieved {kwarg}: ', kwargs[kwarg])

get_results(accuracy=accuracy_score(y_test, y_preds),
            precision=precision_score(y_test, y_preds),
            recall=recall_score(y_test, y_preds),
            f1score=f1_score(y_test, y_preds))
