#
# N. Uras - 21/10/2019
# Support Vector Machine model
#


import os
import joblib
from sklearn.svm import SVC
this_dir, _ = os.path.split(__file__)

class SVM():
    
    
    def __init__(self, model_setup):
        self.model = SVC(**model_setup)
       
        
    def fit_model(self, X_train, y_train):
        # model_setup will a dict with hyperparameters configuration, in my_gridsearch is dict_
        self.model.fit(X_train, y_train)
    
    
    def predict(self, X_test, model_path=None):
        # making predictions
        if model_path:
            saved_model = joblib.load(model_path)
            preds = saved_model.predict(X_test)
        else:
            preds = self.model.predict(X_test)
        
        return preds
