#
# N. Uras - 25 Jan 2021
# Test file of ml_function module
#

import os
import pandas as pd
from ml_function import ComputeMlFunction
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('ml_folder', 'create_dataset')

# open dataset
df_name = 'working_dataset_sample.csv'
df = pd.read_csv(os.path.join(data_dir, f'{df_name}'))

df = df.head(300)

# creating dict with ml_function attributes
params = params = {'C': 1, 'kernel': 'rbf', 'gamma': 'scale'}
model_conf = {'test_size': 0.3,
              'model': 'svm',
              'model_params': params,
              'alfa_os': 0.4,
              'alfa_us': 0.7}
ml_function_instance = ComputeMlFunction(**model_conf)
results = ml_function_instance(df)
