#
# N. Uras - 25 Jan 2021
# ML function which aggregates all the individual parts and 
# get the results
#


import os
import sys
import joblib
from importlib import import_module
this_dir, _ = os.path.split(__file__)
sys.path.insert(0, this_dir.replace('ml_folder', 'oversampling'))
sys.path.insert(1, this_dir.replace('ml_folder', 'models'))
from smote import BalanceSmote
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, f1_score, recall_score


class ComputeMlFunction(object):
    
    
    def __init__(self, test_size, model, model_params, alfa_os, alfa_us):
        self.test_size = test_size
        self.model = model
        self.model_params = model_params # dict with model's parameters
        self.alfa_os, self.alfa_us = alfa_os, alfa_us
        
        
    def __call__(self, df):
        # encoding benford categorical variable
        encoding_map = {True: 1, False: 0}
        df['benford'] = df['benford'].map(encoding_map)
        # dropping topic_id column
        df.drop(labels='topic_id', axis=1, inplace=True)
        # creating inputs and output
        X, y = df.iloc[:, :-1].values, df['label'].values
        # splitting into train and test
        X_train, X_test, y_train, y_test = train_test_split(X, 
                                                            y, 
                                                            test_size=self.test_size, 
                                                            random_state=42)
        # creating balance smote instance
        smote = BalanceSmote(oversampling_ratio=self.alfa_os,
                             undersampling_ratio=self.alfa_us)
        # balance data
        X_train_balanced, y_train_balanced = smote(X_train, y_train)
        # creating model instance
        model = getattr(import_module(f'{self.model}_module'), self.model.upper())(self.model_params)
        # fitting svm model
        model.fit_model(X_train, y_train)
        # making predictions
        y_preds = model.predict(X_test)
        results = {'accuracy': accuracy_score(y_test, y_preds),
                   'precision': precision_score(y_test, y_preds),
                   'recall': recall_score(y_test, y_preds),
                   'f1-score': f1_score(y_test, y_preds)}
        # saving trained model
        joblib.dump(model, this_dir.replace('ml_folder', f'models/trained_models/{self.model}.pkl'))
        
        return results
