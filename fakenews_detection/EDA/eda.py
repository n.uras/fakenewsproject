#
# N. Uras - 22/01/2021
# Exploratory Data Analysis
#

import os
import pandas as pd
import seaborn as sns
from matplotlib.legend_handler import HandlerBase
from matplotlib.text import Text
from matplotlib import pyplot as plt
# this_dir is the directory where working_dataset.py file is located
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('EDA', 'create_dataset/working_dataset_sample.csv')

# open dataset
df = pd.read_csv(data_dir)

df = df.head(50000)

X, y = df.iloc[:, :-1].values, df['label'].values

# splitting into train and test
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, 
                                                    y, 
                                                    test_size=0.3, 
                                                    random_state=42)
y = pd.DataFrame(y_train, columns=['label'])
y.value_counts()

# creating class to handle new labels
class TextHandler(HandlerBase):
    def create_artists(self, legend, tup ,xdescent, ydescent,
                        width, height, fontsize,trans):
        tx = Text(width/2.,height/2,tup[0], fontsize=fontsize,
                  ha="center", va="center", color=tup[1], fontweight="bold")
        return [tx]
    
# sns.set(font_scale=1.5)
ax = sns.countplot(x='label', data=y)
# adding labels to seaborn countplot
handltext = ["1", "0"]
labels = ["fake", "real"]

t = ax.get_xticklabels()
labeldic = dict(zip(handltext, labels))
labels = [labeldic[h.get_text()]  for h in t]
handles = [(h.get_text(),c.get_fc()) for h,c in zip(t,ax.patches)]

ax.legend(handles, labels, handler_map={tuple : TextHandler()}) 
plt.show()
