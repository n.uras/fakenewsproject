#
# N. Uras - 25 Jan 2021
# Test file of BalanceSmote class for oversampling
#

import os
import pandas as pd
from smote import BalanceSmote
from sklearn.model_selection import train_test_split
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('oversampling', 'create_dataset')

# open dataset 
df = pd.read_csv(os.path.join(data_dir, 'working_dataset_sample.csv'))

# df = df.head(100)

# from categorical to numerical
encoding_map = {True: 1, False: 0}
df['benford'] = df['benford'].map(encoding_map)

# dropping topic_id column
df.drop(labels='topic_id', axis=1, inplace=True)

# create inputs and output
X, y = df.iloc[:, :-1].values, df['label'].values

# split df into train and test
X_train, X_test, y_train, y_test = train_test_split(X, 
                                                    y, 
                                                    test_size=0.3, 
                                                    random_state=42)

# Oversampling:
# N_rm = oversampling_ratio*N_M, where N_rm is the number of samples 
# in the minority class after resampling and N_M is the number of samples 
# in the majority class.
# Undersampling:
# N_rM = N_m/undersampling_ratio, where N_rM is the number of samples in 
# the majority class after resampling and is the number of samples in the 
# minority class.
smote = BalanceSmote(oversampling_ratio=0.4,
                     undersampling_ratio=0.7)

# balance data
X_train_balanced, y_train_balanced = smote(X_train, y_train)
