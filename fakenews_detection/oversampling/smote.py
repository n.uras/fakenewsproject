#
# N. Uras - 22 Jan 2021
# This file aims to implement the SMOTE technique in order to 
# oversample our minority class, i.e. the fake class and deal with
# a more balanced class distribution
#


import numpy as np
from collections import Counter
from matplotlib import pyplot as plt
from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler


class BalanceSmote(object):
    
    
    def __init__(self, oversampling_ratio, undersampling_ratio):
        self.oversample = oversampling_ratio
        self.undersample = undersampling_ratio
        
        
    def __call__(self, X_train, y_train):
        # You should always split your dataset into training and 
        # testing sets before balancing the data and balance only the 
        # training one. That way, you ensure that the test dataset is 
        # as unbiased as it can be and reflects a true evaluation 
        # for your model.
        over_s = SMOTE(sampling_strategy=self.oversample)
        under_s = RandomUnderSampler(sampling_strategy=self.undersample)
        pipe = Pipeline(steps=[('oversample', over_s), ('undersample', under_s)])
        # transforming datasets into balanced ones
        X, y = pipe.fit_resample(X_train, y_train)
        
        return X, y
    
    
    def plot_balanced_data(self, balanced_X, balanced_y):
        counter = Counter(balanced_y)
        print(counter)
        for label, _ in counter.items():
            	row_ix = np.where(balanced_y == label)[0]
            	plt.scatter(balanced_X[row_ix, 0], balanced_X[row_ix, 1], label=str(label))
        plt.legend()
        plt.show()
        